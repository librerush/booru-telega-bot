;; -*- mode: scheme; coding: utf-8 -*-
#!r6rs

(library (telega.ss)
  (export bot-token
          poll-updates
          message?
          edited-message?
          callback-query?
          send-message
          send-document
          send-photo
          vector-assoc)
  (import (rnrs (6))
          (private coroutine)
          (curl-scheme)
          (json)
          (only (chezscheme)
                format
                getenv
                pretty-print))

  ;; FIXME: rewrite
  (define (vector-assoc obj-sym vec)
    (define obj (map symbol->string obj-sym))
    (if (vector? vec)
        (let loop ((o obj) (v vec))
          (if (null? (cdr o)) ; last?
              (let lp ((i 0))
                (if (< i (vector-length v))
                    (let ((e (vector-ref v i)))
                      (if (equal? (car e) (car o))
                          (cdr e)
                          (lp (+ 1 i))))
                    #f))
              (let lp ((i 0))
                (if (< i (vector-length v))
                    (let ((e (vector-ref v i)))
                      (if (equal? (car e) (car o))
                          (if (vector? (cdr e))
                              (loop (cdr o) (cdr e))
                              #f)
                          (lp (+ 1 i))))
                    #f))))
        #f))

  (define connection #f)

  (define api-url "https://api.telegram.org/")

  (define bot-token
    (string-append
     "bot"
     (or (getenv "TELEGA_TOKEN")
         (error 'bot-token "Set TELEGA_TOKEN env variable!"))
     "/"))

  (define-syntax define-method
    (syntax-rules ()
      ((_ method-name method-name-str (var1 ... varn))
       (define method-name
         (lambda (var1 ... varn)
           (let* ((vals (list var1 ... varn))
                  (vars (list 'var1 ... 'varn))
                  (url (format "~a~a~a?~a"
                               api-url
                               bot-token
                               method-name-str
                               (fold-right
                                (lambda (var val rest)
                                  (if (null? rest)
                                      (format "~a=~a" var val)
                                      (format "~a=~a&~a" var val rest)))
                                '()
                                vars vals))))
             (unless connection
               (set! connection (http-open-connection)))
             (json-read
              (open-string-input-port
               (utf8->string
                (http-response-port (http/get connection url)))))))))))

  (define-method get-updates "getUpdates" (offset timeout))

  (define-method send-message "sendMessage" (chat_id text reply_markup))

  (define-method send-photo "sendPhoto" (chat_id photo caption reply_markup))

  (define-method send-document "sendDocument" (chat_id document reply_to_message_id))

  (define (update-type? type update)
    (and (vector-assoc type update)
         #t))

  (define (message? update)
    (update-type? '(message) update))

  (define (edited-message? update)
    (update-type? '(edited_message) update))

  (define (callback-query? update)
    (update-type? '(callback_query) update))

  (define (poll-updates handler)
    (set! connection (http-open-connection))
    (let ((offset 0))
      (let lp ()
        (for-each (lambda (update)
                    (handler update)
                    (set! offset
                      (+ 1 (vector-assoc '(update_id) update))))
                  (vector-assoc '(result)
                                (get-updates offset 50)))
        (lp)))
    (http-close-connection! connection))
  )
