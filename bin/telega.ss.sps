#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
#!r6rs

(import (rnrs (6))
        (telega.ss)
        (private coroutine)
	(only (chezscheme)
              current-time
              date-and-time
              display-statistics
              format
              printf
              getenv
	      pretty-print
              make-time
              trace
              trace-lambda
              trace-let
              time-difference
              time-nanosecond
              sleep
              with-output-to-string)
        (only (srfi :13) string-map)
        (srfi :115 regexp)
        (curl-scheme)
        (json))

(define tuser
  (or (getenv "TELEGA_USER")
      (error 'tuser "Set TELEGA_USER env variable!")))

(define (allowed-tuser? chat-id)
  (= (string->number tuser)
     chat-id))

(define deny-tags
  (regexp-split
   '(+ space)
   (or (getenv "DENY_TAGS")
       (error 'deny-tags "Set DENY_TAGS env variable!"))))

(define (llog . msg)
  (printf "~a -- ~a\n"
          (date-and-time)
          msg)
  (flush-output-port (current-output-port)))


;;; Booru
(define booru-api "https://danbooru.donmai.us/")

(define (http/get-json conn url)
  (json-read
   (open-string-input-port
    (utf8->string
     (http-response-port (http/get conn url))))))

(define (get-post-by-id conn id)
  (http/get-json conn (format "~aposts/~a.json"
                              booru-api id)))

;; Returns parsed json
(define (get-posts-by-tag-limit conn tags limit)
  (http/get-json conn (format "~aposts.json?tags=~a&limit=~a"
                              booru-api tags limit)))

(define (object->json-string o)
  (with-output-to-string
    (lambda ()
      (json-write o))))

;; Returns json string
(define (make-inline-keyboard texts data)
  (let ((v `#(("inline_keyboard" .
               ((,@(map
                    (lambda (t d)
                      `#(("text" . ,t)
                         ("callback_data" . ,d)))
                    texts data)))))))
    (object->json-string v)))

;; Returns json string
(define (make-reply-keyboard texts)
  (let ((v `#(("keyboard" .
               ((,@(map (lambda (t) `#(("text" . ,t))) texts))))
              ("resize_keyboard" . #t)
              ("one_time_keyboard" . #t))))
    (object->json-string v)))

(define (denied-tags? tag-string)
  (if (regexp-search `(or ,@deny-tags) tag-string)
      #t
      #f))

;; Send pic-num pictures
(define (send-pictures chat-id tags limit pic-num)
  (let* ((conn (http-open-connection))
         (json-resp
          (call/cc
           (lambda (cont)
             (with-exception-handler
                 (lambda (exn)
                   (llog (condition-message exn)
                         (condition-irritants exn)
                         "json-resp")
                   (cont #f))
               (lambda ()
                 (reverse (get-posts-by-tag-limit conn tags limit)))))))
         (num 0))
    (when json-resp
      (let ((coroutines
             (map
              (lambda (vec)
                (coroutine
                 (lambda ()
                   (let ((current-t (current-time))
                         (id (vector-assoc '(id) vec))
                         (fav-count (vector-assoc '(fav_count) vec))
                         (file-url (vector-assoc '(file_url) vec))
                         (tag-string-artist (vector-assoc '(tag_string_artist) vec))
                         (tag-string-general (vector-assoc '(tag_string_general) vec)))
                     (when (and (> fav-count 20)
                                (string? file-url)
                                (string? tag-string-artist)
                                (string? tag-string-general)
                                (< num pic-num)
                                (not (denied-tags? tag-string-general)))
                       (let* ((reply-str (make-inline-keyboard
                                          (list "🔥" "🔍")
                                          (list (string-append
                                                 "get-" ; e.g. get-12345
                                                 (number->string id))
                                                (string-append
                                                 "search-" ; e.g. search-12345
                                                 (number->string id))))))
                         (when (> (time-nanosecond
                                   (time-difference current-t (current-time)))
                                  1000000)
                           (yield #t))
                         (send-photo chat-id file-url tag-string-artist reply-str)
                         (set! num (+ 1 num))))
                     'end)
                   )))
              json-resp)))
        (map (lambda (cort)
               (let lp ((next (continue cort)))
                 (unless (eq? 'end next)
                   (lp (continue cort))))
               #t)
             coroutines)
        (http-close-connection! conn)
        #t))))

(define (handle-callback-data chat-id msg-id data)
  (define conn (http-open-connection))
  (define (matches? beginning)
    (regexp-matches `(: bos ,beginning (+ digit))
                    data))
  (cond ((matches? "get-")
         (let* ((post
                 (get-post-by-id
                  conn
                  (car (regexp-extract '(+ digit)
                                       data))))
                (file-url (vector-assoc '(file_url) post)))
           (when file-url
             (http-close-connection! conn)
             (send-document chat-id file-url msg-id))))
        ((matches? "search-")
         (let* ((post
                 (get-post-by-id
                  conn
                  (car (regexp-extract '(+ digit)
                                       data))))
                (tag-string-character
                 (vector-assoc '(tag_string_character) post))
                (tag-string-artist
                 (vector-assoc '(tag_string_artist) post))
                (tag (if (and (string? tag-string-artist)
                              (string? tag-string-character))
                         (string-append tag-string-artist " "
                                        tag-string-character)
                         #f)))
           (when tag
             (http-close-connection! conn)
             (send-message chat-id "Please, select:"
                           (make-reply-keyboard
                            (regexp-split '(+ whitespace)
                                          tag))))))
        (else (llog "No such callback for" data))))


(define (handler update)
  (cond ((message? update)
         (let ((chat-id (vector-assoc '(message from id) update))
               (text (vector-assoc '(message text) update)))
           (llog (vector-assoc '(message from first_name) update)
                 chat-id
                 "->"
                 text)
           (when (allowed-tuser? chat-id)
             (call/cc
              (lambda (cont)
                (with-exception-handler
                    (lambda (exn)
                      (llog (condition-message exn)
                            (condition-irritants exn)
                            (if (who-condition? exn)
                                (condition-who exn)
                                "handler (send-pictures)"))
                      (cont #f))
                  (lambda ()
                    (if (equal? "/stats" text)
                        (send-message chat-id
                                      (escape
                                       (with-output-to-string
                                         (lambda ()
                                           (display-statistics))))
                                      "")
                        (send-pictures chat-id (escape text) 120 7)))))))))
        ((callback-query? update)
         (call/cc
          (lambda (cont)
            (with-exception-handler
                (lambda (exn)
                  (llog (condition-message exn)
                        (condition-irritants exn))
                  (cont #f))
              (lambda ()
                (let ((callback-data (vector-assoc
                                      '(callback_query data) update))
                      (chat-id (vector-assoc
                                '(callback_query from id) update))
                      (msg-id (vector-assoc
                               '(callback_query message message_id) update)))
                  (when (allowed-tuser? chat-id)
                    (llog (vector-assoc '(callback_query from first_name) update)
                          chat-id
                          "callback data ->"
                          callback-data)
                    (handle-callback-data chat-id msg-id callback-data))))))))
        (else #f)))

(define (main args)
  (let main-loop ()
    (call/cc
     (lambda (cont)
       (with-exception-handler
           (lambda (exn)
             (llog (condition-message exn)
                   (condition-irritants exn)
                   (if (who-condition? exn)
                       (condition-who exn)
                       ""))
             (cont #f))
         (lambda ()
           (poll-updates handler)))))
    (llog "main-loop: sleep for 100 seconds")
    (sleep (make-time 'time-duration 0 100)) ; sleep for 100 seconds
    (main-loop)))

(main (command-line))
