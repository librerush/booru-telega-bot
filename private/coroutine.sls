;; -*- mode: scheme; coding: utf-8 -*-
#!r6rs

(library (private coroutine)
  (export let/cc
          coroutine
          coroutine?
          continue
          yield)
  (import (rnrs)
          (srfi :26 cut))

  (define-syntax let/cc
    (syntax-rules ()
      ((_ id body ...)
       (call/cc (lambda (id) body ...)))))

  (define current-coroutine
    (lambda ()
      #f))

  (define-record-type cort
    (fields
     (mutable k)
     (mutable end?)
     (mutable res)))

  (define (coroutine-start! k thunk)
    (let ((c (make-cort #f #f #f)))
      (set! current-coroutine (lambda () c))
      (let/cc kk
              (cort-k-set! c kk)
              (k c))
      (call-with-values thunk
        (lambda res
          (let ((k (cort-k c)))
            (cort-end?-set! c #t)
            (cort-res-set! c res)
            (cort-k-set! c #f)
            (apply k res))))))

  (define (coroutine-continue! c args)
    (if (cort-end? c)
        (apply values (cort-res c))
        (let ((k (cort-k c)))
          (let/cc kk
                  (cort-k-set! c kk)
                  (apply k args)))))

  (define (coroutine-yield! c args)
    (let ((k (cort-k c)))
      (if (cort-end? c)
          (apply values (cort-res c))
          (let/cc kk
                  (cort-k-set! c kk)
                  (apply k args)))))

  (define (coroutine? c)
    (cort? c))

  (define (continue c . args)
    (if (coroutine? c)
        (coroutine-continue! c args)
        (error "Illegal argument; not continuable" c)))

  (define yield
    (lambda args
      (cond ((current-coroutine)
             => (cut coroutine-yield! <> args))
            (else
             (error "Not in a coroutine continuation" 'yield)))))

  (define (coroutine proc . args)
    (let/cc k
            (coroutine-start! k
                              (if (null? args)
                                  proc
                                  (cut apply proc args)))))
)
